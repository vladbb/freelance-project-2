@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-2">
            @include('templates.side_menu')
        </div>
        <div class="col">
            <div class="profile-content p-3">
                <form action="{{ route('order.store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group mb-3">
                                <label for="title" class="form-label">Title</label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title">
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="description" class="form-label">Description</label>
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="5"></textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="price" class="form-label">Desired budget</label>
                                <input type="text" class="form-control @error('price') is-invalid @enderror" name="price" id="price">
                                @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-3">
                                        <label for="term" class="form-label">Term</label>
                                        <select class="form-select" id="term" name="term">
                                            <option value="1">1 day</option>
                                            <option value="2">2 day</option>
                                            <option value="3">3 day</option>
                                            <option value="7">1 week</option>
                                            <option value="14">2 week</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group mb-3">
                                        <label for="category" class="form-label">Category</label>
                                        <select class="form-select" id="category" name="category_id">
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection