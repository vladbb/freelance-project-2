@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-2">
            @include('templates.side_menu')
        </div>
        <div class="col">
            <div class="profile-content p-3 mb-4">
                <a href="{{ route('order.create') }}" class="h5 text-decoration-none fw-bold">+ Add new order</a>
            </div>
            @foreach($orders as $order)
                <div class="profile-content p-3 mb-4">
                    <p class="h4 fw-bold">{{ $order->title }}</p>
                    <p>{{ $order->description }}</p>
                    <p class="h5 fw-bold">Бюджет проекта до {{ $order->price }} р</p>
                    <p>Срок {{ $order->term }}дн</p>
                    <div class="row">
                        <div class="col-7">
                            <small>Автор: User-1</small>
                        </div>
                        <div class="col-2 text-md-end">
                            <a href="{{ route('order.show', $order->id) }}" class="btn btn-outline-primary">Responses</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('order.edit', $order->id) }}" class="btn btn-outline-primary">Edit</a>
                        </div>
                        <div class="col-1">
                            <form action="{{ route('order.delete', $order->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-outline-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection