@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-2">
            @include('templates.side_menu')
        </div>
        <div class="col">
            @foreach($responses as $response)
                <div class="profile-content p-3 mb-4">
                    <p class="h4 fw-bold">{{ $response->title }}</p>
                    <p>{{ $response->description }}</p>
                    <p class="h5 fw-bold">Бюджет проекта до {{ $response->price }} р</p>
                    <p>Срок {{ $response->term }}дн</p>
                    <div class="row">
                        <div class="col-7">
                            <small>Автор: User-1</small>
                        </div>
                        <div class="col-2 text-md-end">
                            <a href="{{ route('order.show', $response->id) }}" class="btn btn-outline-primary">View Order</a>
                        </div>
                        <div class="col-1">
                            <a href="{{ route('order.edit', $response->id) }}" class="btn btn-outline-primary">Edit</a>
                        </div>
                        <div class="col-1">
                            <form action="{{ route('order.delete', $response->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-outline-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection