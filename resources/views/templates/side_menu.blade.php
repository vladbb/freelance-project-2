<div class="d-flex flex-column flex-shrink-0 p-3 text-dark bg-white side-menu">
    <span class="ml-4 h5">User-1</span>
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
        <li class="nav-item">
            <a href="{{ route('user.index') }}" class="nav-link {{ str_contains(Request::path(), 'user') ? 'active' : '' }} fw-bold" aria-current="page">Settings</a>
        </li>
        @if(Auth::user()->role === 'customer')
            <li class="nav-item">
                <a href="{{ route('order.index') }}" class="nav-link {{ str_contains(Request::path(), 'orders') ? 'active' : '' }} fw-bold" aria-current="page">Orders</a>
            </li>
        @endif
        @if(Auth::user()->role === 'executor')
            <li class="nav-item">
                <a href="{{ route('response.index') }}" class="nav-link {{ str_contains(Request::path(), 'responses') ? 'active' : '' }} fw-bold" aria-current="page">Responses</a>
            </li>
        @endif
    </ul>
</div>