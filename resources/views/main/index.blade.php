@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-2">
        @auth('web')
            @include('templates.side_menu')
        @endauth
        </div>
        <div class="col">
        @foreach($orders as $order)
                <div class="profile-content p-3 mb-4">
                    <p class="h4 fw-bold">{{ $order->title }}</p>
                    <p>{{ $order->description }}</p>
                    <p class="h5 fw-bold">Бюджет проекта до {{ $order->price }} р</p>
                    <p>Срок {{ $order->term }}дн</p>
                    <div class="row">
                        <div class="col-10">
                            <small>Автор: User-1</small>
                        </div>
                        <div class="col-1 text-md-end">
                            <a href="{{ route('order.edit', $order->id) }}" class="btn btn-outline-primary">Откликнуться</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    
@endsection 