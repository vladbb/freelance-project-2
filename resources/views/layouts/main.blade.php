<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Freelance</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="wrapper">
        <div class="header bg-white mb-4">
            <div class="container">
                <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4">
                    <a href="/" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
                        Freelance.test
                    </a>

                    <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
                        <li><a href="{{ route('main.index') }}" class="nav-link px-2 link-dark">Home</a></li>
                        <li><a href="#" class="nav-link px-2 link-dark">Orders</a></li>
                        <li><a href="#" class="nav-link px-2 link-dark">Chat</a></li>
                        <li><a href="#" class="nav-link px-2 link-dark">About</a></li>
                    </ul>

                    @auth('web')
                        <div class="col-md-3 text-end">
                            <a href="{{ route('user.index') }}" class="btn btn-outline-primary me-2">Profile</a>
                            <a class="btn btn-primary" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    @endauth

                    @guest('web')
                        <div class="col-md-3 text-end">
                            <a href="{{ route('login') }}" class="btn btn-outline-primary me-2">Login</a>
                            <a href="{{ route('register') }}" class="btn btn-primary">Sign-up</a>
                        </div>
                    @endguest

                    
                </header>
            </div>
        </div>

        <main class="main">
            <section class="content">
                <div class="container">
                    @yield('content')
                </div>
            </section>
        </main>
    
        <div class="footer bg-white">
            <div class="container">
                <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-2">
                    <p class="col-md-4 mb-0 text-muted">© 2021 Company, Inc</p>

                    <a href="/" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
                        Freelance.test
                    </a>

                    <ul class="nav col-md-4 justify-content-end">
                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Home</a></li>
                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Orders</a></li>
                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Chat</a></li>
                        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">About</a></li>
                    </ul>
                </footer>
            </div>
        </div>
    </div>
</body>
</html>