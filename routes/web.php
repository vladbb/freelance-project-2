<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Main'], function() {
    Route::get('/', 'IndexController')->name('main.index');
});

Route::middleware("auth")->group(function() {
    Route::group(['namespace' => 'Profile', 'prefix' => 'user'], function() {
        Route::get('/', 'IndexController')->name('user.index'); 
    });

    Route::group(['namespace' => 'Response', 'prefix' => 'responses'], function() {
        Route::get('/', 'IndexController')->name('response.index'); 
    });

    Route::group(['namespace' => 'Order', 'prefix' => 'orders'], function() {
        Route::get('/', 'IndexController')->name('order.index');
        Route::get('/create', 'CreateController')->name('order.create');
        Route::post('/', 'StoreController')->name('order.store');
        Route::get('/{order}', 'ShowController')->name('order.show');
        Route::get('/{order}/edit', 'EditController')->name('order.edit');
        Route::patch('/{order}', 'UpdateController')->name('order.update');
        Route::delete('/{order}', 'DeleteController')->name('order.delete');
    });
});

Auth::routes();
