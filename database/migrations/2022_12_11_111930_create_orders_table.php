<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('price')->nullable();
            $table->string('term')->nullable();

            $table->unsignedBigInteger('category_id')->nullable();
            $table->index('category_id', 'order_category_idx');
            $table->foreign('category_id', 'order_category_fk')->on('categories')->references('id');

            $table->string('status')->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->index('user_id', 'order_user_idx');
            $table->foreign('user_id', 'order_user_fk')->on('users')->references('id');

            $table->unsignedBigInteger('executor')->nullable();
            $table->index('executor', 'order_user_idx2');
            $table->foreign('executor', 'order_user_fk2')->on('users')->references('id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
