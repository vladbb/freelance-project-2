<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('price')->nullable();
            $table->string('term')->nullable();
            $table->string('status')->nullable();

            $table->unsignedBigInteger('order_id')->nullable();
            $table->index('order_id', 'response_order_idx');
            $table->foreign('order_id', 'response_order_fk')->on('orders')->references('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->index('user_id', 'response_user_idx');
            $table->foreign('user_id', 'response_user_fk')->on('users')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
