<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Response;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'description',
        'price',
        'term',
        'category_id',
        'status',
        'user_id',
        'executor',
    ];

    protected $quarded = false;

    public function responses()
    {
        return $this->hasMany(Response::class);
    }
}
