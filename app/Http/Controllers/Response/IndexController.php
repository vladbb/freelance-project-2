<?php

namespace App\Http\Controllers\Response;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Response;

class IndexController extends Controller
{
    public function __invoke()
    {
        if (Auth::user()->role === 'executor') {
            $responses = Response::all();
            $responses = $responses->where('user_id', Auth::id());

            return view('response.index', compact('responses')); 
        }
        return redirect()->route('main.index');
    }
}
