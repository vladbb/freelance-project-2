<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class DeleteController extends Controller
{
    public function __invoke(Order $order)
    {  
        if (Auth::user()->role === 'customer') {
            $order->delete();
        
            return redirect()->route('order.index');
        }
        return redirect()->route('main.index');
    }
}
