<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Order\StoreRequest;
use App\Models\Order;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        if (Auth::user()->role === 'customer') {
            $data = $request->validated();

            $data['status'] = 'waiting';
            $data['user_id'] = Auth::id();
            
            Order::create($data);

            return redirect()->route('order.index'); 
        }
        return redirect()->route('main.index');
    }
}
