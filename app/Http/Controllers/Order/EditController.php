<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class EditController extends Controller
{
    public function __invoke(Order $order)
    {  
        if (Auth::user()->role === 'customer') {
            return view('order.edit', compact('order'));
        }
        return redirect()->route('main.index');
    }
}
