<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;

class IndexController extends Controller
{
    public function __invoke()
    {
        if (Auth::user()->role === 'customer') {
            $orders = Order::all();
            $orders = $orders->where('user_id', Auth::id());
            
            return view('order.index', compact('orders'));
        }
        return redirect()->route('main.index');
    }
}
