<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CreateController extends Controller
{
    public function __invoke()
    {
        if (Auth::user()->role === 'customer') {
           return view('order.create'); 
        }
        return redirect()->route('main.index');
    }
}
