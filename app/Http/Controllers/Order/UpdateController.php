<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Http\Requests\Order\UpdateRequest;
use Illuminate\Support\Facades\Auth;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Order $order)
    {
        if (Auth::user()->role === 'customer') {
            $data = $request->validated();
            $order->update($data);

            return redirect()->route('order.index');
        }
        return redirect()->route('main.index');
    }
}
