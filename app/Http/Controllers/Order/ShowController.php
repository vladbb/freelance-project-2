<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class ShowController extends Controller
{
    public function __invoke(Order $order)
    {  
        if (Auth::user()->role === 'customer') {
            $responses = $order->responses()->get();

            return view('order.show', compact('order', 'responses'));
        }
        return redirect()->route('main.index');
    }
}
